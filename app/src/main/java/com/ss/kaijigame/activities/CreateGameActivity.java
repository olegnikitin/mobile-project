package com.ss.kaijigame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.ss.kaijigame.R;
import com.ss.kaijigame.enums.BOT_TYPE;
import com.ss.kaijigame.utils.Resources;
import com.ss.kaijigame.utils.UpdateTaskCreateGame;


public class CreateGameActivity extends Activity {

    private EditText nameGameEditText, numberOfCardsEditText, numberOfStarsEditText;

    private boolean isBotGame;
    private BOT_TYPE botType;

    //todo send it to Service
    private UpdateTaskCreateGame updateTaskCreateGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);
        nameGameEditText = (EditText) findViewById(R.id.enterYourNameGameEditText);
        numberOfCardsEditText = (EditText) findViewById(R.id.numberOfCardsEditText);
        numberOfStarsEditText = (EditText) findViewById(R.id.numberOfStarsEditText);
        updateTaskCreateGame = new UpdateTaskCreateGame(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onPlayerChoise(View view) {
        Button playerB = (Button) findViewById(R.id.playerGameRadioButton);
        if(playerB.isActivated()){
            numberOfStarsEditText.setVisibility(View.VISIBLE);
        }else{
            numberOfStarsEditText.setVisibility(View.INVISIBLE);
        }
    }

    public void onRadioClick(View view) {
        switch (view.getId()) {
            case R.id.playerGameRadioButton:
                isBotGame = false;
                botType = null;
                break;
            case R.id.EASY_BOT:
                isBotGame = true;
                botType = BOT_TYPE.EASY;
                break;
            case R.id.MEDIUM_BOT:
                isBotGame = true;
                botType = BOT_TYPE.MEDIUM;
                break;
            case R.id.HARD_BOT:
                isBotGame = true;
                botType = BOT_TYPE.HARD;
                break;
            default:
                break;
        }
    }

    private String createGame(boolean isBotGame, BOT_TYPE botType){
        if(isBotGame){
            StringBuffer requestSB = new StringBuffer();
            requestSB.append("http://192.168.0.80:8080/ROOT/rest/botgame/create?name=");
            requestSB.append(Resources.getUserName());
            requestSB.append("&gamename=" + nameGameEditText.getText().toString());
            requestSB.append("&cards=" + numberOfCardsEditText.getText().toString());
            requestSB.append("&bot=" + botType);
            String str = new String(requestSB);
            return str;
        }else{
            StringBuffer requestSB = new StringBuffer();
            requestSB.append("http://192.168.0.80:8080/ROOT/rest/playergame/create?name=");
            requestSB.append(Resources.getUserName());
            requestSB.append("&gamename=" + nameGameEditText.getText().toString());
            requestSB.append("&cards=" + numberOfCardsEditText.getText().toString());
            requestSB.append("&stars=" + numberOfStarsEditText.getText().toString());
            requestSB.toString();
            String str = new String(requestSB);
            return str;
        }
    }

    public void onButtonClick(View view) {
        updateTaskCreateGame.loadJSON(this.createGame(isBotGame, botType)); //todo send to service
        Intent intent = new Intent(this, ChooseGameActivity.class);
        startActivity(intent);
    }
}
