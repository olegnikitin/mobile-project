package com.ss.kaijigame.model;

import com.ss.kaijigame.enums.BOT_TYPE;

/**
 * Created by Oleg Nikitin on 07.09.2014.
 */
public class Game {
    private String nameGame;
    private Integer numberOfCards;
    private BOT_TYPE bot_type;
    private Player player;

    public String getNameGame() {
        return nameGame;
    }

    public void setNameGame(String nameGame) {
        this.nameGame = nameGame;
    }

    public Integer getNumberOfCards() {
        return numberOfCards;
    }

    public void setNumberOfCards(Integer numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    public BOT_TYPE getBot_type() {
        return bot_type;
    }

    public void setBot_type(BOT_TYPE bot_type) {
        this.bot_type = bot_type;
    }

    public Player getPlayer() {

        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
