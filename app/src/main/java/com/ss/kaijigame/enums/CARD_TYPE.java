package com.ss.kaijigame.enums;

/**
 * Created by Oleg Nikitin on 07.09.2014.
 */
public enum CARD_TYPE {
    ROCK, PAPER, SCISSORS
}
