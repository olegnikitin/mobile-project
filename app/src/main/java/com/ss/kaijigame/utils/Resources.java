package com.ss.kaijigame.utils;

import com.ss.kaijigame.model.Game;

import java.util.ArrayList;
import java.util.List;

public class Resources {
    private static String userName;
    //http://suvitruf.ru/2013/06/11/3243/

    private static List<Game> listOfGames = new ArrayList<Game>();
    public static void addGame(Game game){
        listOfGames.add(game);
    }

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        Resources.userName = userName;
    }

    public static List<Game> getListOfGames() {
        return listOfGames;
    }
}
