package com.ss.kaijigame.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ss.kaijigame.R;

public class PlayGameActivity extends Activity {

    private ImageButton rockButton, paperButton, scissorsButton;
    private TextView oppRock, oppPaper, oppSc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        rockButton = (ImageButton) findViewById(R.id.rockImageButton);
        paperButton = (ImageButton) findViewById(R.id.paperImageButton);
        scissorsButton = (ImageButton) findViewById(R.id.scissorsImageButton);

        //opponent's cards
        oppRock = (TextView) findViewById(R.id.oppRockCardsTextView);
        oppPaper = (TextView) findViewById(R.id.oppPaperCardsTextView);
        oppSc = (TextView) findViewById(R.id.oppScissorsCardTextView);
        //todo add the values of your cards and opponent. the information in Resources
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.play_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
