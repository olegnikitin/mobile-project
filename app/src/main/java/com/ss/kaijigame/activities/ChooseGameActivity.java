package com.ss.kaijigame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.ss.kaijigame.R;
import com.ss.kaijigame.model.Game;
import com.ss.kaijigame.utils.Resources;


public class ChooseGameActivity extends Activity {

    private Button botGameButton, playerGameButton;
    private ListView listOfGames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_game);
        botGameButton = (Button) findViewById(R.id.selectBotGameButton);
        playerGameButton = (Button) findViewById(R.id.selectPlayerGameButton);
        listOfGames = (ListView) findViewById(R.id.selectExistingGameListView);

        ArrayAdapter<Game> adapter = new ArrayAdapter<Game>(this,android.R.layout.simple_list_item_single_choice, Resources.getListOfGames());
        listOfGames.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.choose_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToBotActivity(View view) {
        Intent intent = new Intent(this, PlayGameActivity.class);
        startActivity(intent);
    }

    public void goToPlayerActivity(View view) {
        //TODO change to workable activity
        Intent intent = new Intent(this, ChooseGameActivity.class);
        startActivity(intent);
    }
}
