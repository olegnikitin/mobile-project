package com.ss.kaijigame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ss.kaijigame.R;
import com.ss.kaijigame.utils.Resources;


public class LoginActivity extends Activity {

    private EditText login;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login = (EditText) findViewById(R.id.editTextLogin);
        Resources.userName = login.getText().toString();
        loginButton = (Button) findViewById(R.id.loginButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToMenu(View view) {
        Intent intent = new Intent(this, ChooseGameActivity.class);
        if(Resources.userName != null){
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(), "Enter your login!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Test method(Unworkable)
     * @param something
     */
    public void updateGUI(String something){

    }


}
