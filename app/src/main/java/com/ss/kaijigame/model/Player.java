package com.ss.kaijigame.model;

/**
 * Created by Oleg Nikitin on 07.09.2014.
 */
public class Player {
    private String nameOfPlayer;
    private Integer numberOfRocks, numberOfPaper, numberOfScrissors, numberOfStars;
    private boolean isBot;

    public String getNameOfPlayer() {
        return nameOfPlayer;
    }

    public void setNameOfPlayer(String nameOfPlayer) {
        this.nameOfPlayer = nameOfPlayer;
    }

    public Integer getNumberOfRocks() {
        return numberOfRocks;
    }

    public void setNumberOfRocks(Integer numberOfRocks) {
        this.numberOfRocks = numberOfRocks;
    }

    public Integer getNumberOfPaper() {
        return numberOfPaper;
    }

    public void setNumberOfPaper(Integer numberOfPaper) {
        this.numberOfPaper = numberOfPaper;
    }

    public Integer getNumberOfScrissors() {
        return numberOfScrissors;
    }

    public void setNumberOfScrissors(Integer numberOfScrissors) {
        this.numberOfScrissors = numberOfScrissors;
    }

    public Integer getNumberOfStars() {
        return numberOfStars;
    }

    public void setNumberOfStars(Integer numberOfStars) {
        this.numberOfStars = numberOfStars;
    }

    public boolean isBot() {
        return isBot;
    }

    public void setBot(boolean isBot) {
        this.isBot = isBot;
    }
}
