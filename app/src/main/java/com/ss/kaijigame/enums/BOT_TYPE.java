package com.ss.kaijigame.enums;

/**
 * Created by Oleg Nikitin on 07.09.2014.
 */
public enum BOT_TYPE {
    EASY, MEDIUM, HARD
}
